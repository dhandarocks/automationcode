package com.spring.demo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainAppXml {

    public static void main(String[] args) {

        //Create the Spring Container
        ClassPathXmlApplicationContext context=new ClassPathXmlApplicationContext("BasicConfiguration.xml");

        //Retrieve the bean
        BasketballCoach coach=context.getBean("myCoach",BasketballCoach.class);

        Coach coach1=context.getBean("cricket",Coach.class);

        //call the methods
        System.out.println(coach.getDailyWorkout());

        System.out.println(coach1.getDailyWorkout());

        System.out.println(coach.newMethod());

        //close the container
        context.close();
    }
}
