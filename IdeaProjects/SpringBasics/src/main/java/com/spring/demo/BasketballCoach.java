package com.spring.demo;

public class BasketballCoach implements Coach{

    @Override
    public String getDailyWorkout()
    {
        return "run for atleast 10 kms on daily basis";
    }

    public String newMethod()
    {
        return "hello";
    }
}
